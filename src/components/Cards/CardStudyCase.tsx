interface ICard {
  image: string
  nameCompany: string
  title: string
  webIcon?: string
  androidIcon?: string
  iosIcon?: string
}

function CardStudyCase(props: ICard) {
  const { image, nameCompany, title, webIcon, androidIcon, iosIcon } = props
  return (
    <div className="w-[411px] h-[431px] rounded-[12px]">
      <img className="rounded-[12px]" src={image} alt="" />
        <div className="flex justify-between items-center mt-4">
          <p className="font-medium text-[#A1A5B7]">{nameCompany}</p>
          <div className="flex gap-x-2 -mt-4">
            <img src={webIcon} alt="" />
            <img src={androidIcon} alt="" />
            <img className="mb-1" src={iosIcon} alt="" />
          </div>
        </div>
      <h6 className="text-[20px] text-[#404258] font-bold">{title}</h6>
    </div>
  )
}

export default CardStudyCase
